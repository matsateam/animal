package net.ukr.wumf;

public class Main {

	public static void main(String[] args) {

		Veterinarian vet = new Veterinarian("Dmytro");
		Cat cat = new Cat("milk", "red", 3, "Murzik");
		Dog dog = new Dog("meat", "black", 10, "Polkan");

		System.out.println(vet.getName());
		System.out.println(dog.toString());
		System.out.println(cat.toString());

		vet.treatment(dog);

	}

}
